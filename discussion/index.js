db.users.insertOne({
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
        phone: "12345678",
        email: "bill@gmail.com"
        },
        courses: ["PHP","Laravel","HTML"],
        department: "Operations",
        status: "active"      
})
// look for document with age less than 50
db.users.find(
    { age: { $lt: 50 } }
)
// look for document with age greater than or equal
db.users.find(
    { age: { $gte: 50 } }
)
// look for document with age not equal to 82
db.users.find(
    { age: { $ne: 82 } }
)

//look for documents that has last names Hawking and Doe

db.users.find(
        {
                lastName: { $in: ["Hawking", "Doe"]}
        }
);

db.users.find(
        {
                courses: { $in: ["HTML", "React"]}
        }
);

db.users.find(
        {
                        $or: 
                                [ {firstName: "Neil"},
                                {age: { $gt: 30}}
                ]
        }
);

db.users.find(
        {
                $and: [
                        {age: {$ne:82}},
                        {age: {$ne:76}}
                ]
        }
);



//look for a document that has a name Jane and exclude contact and department dates



//look for a document that has a name Jane and exclude contact and department dates

db.users.find(
        {
                "lastName":"Jane"
        },
        {
                "contact": 0,
                "department": 0
        });
//look for a document that has a name neil and exlude id but include firstName, lastName and contact.
db.users.find(
        {firstName: "Neil"},
        {
                _id: 0,
                firstName: 1,
                lastName: 1,
                contact: 1
        }        
);

db.users.find(
        {firstName: "Bill"},
        {
                _id: 0,
                firstName: 1,
                lastName: 1,
                "phone number": 1
        }        
);
//Suppressing specific fiends in embedded documents
// look for a document that has a firstName Bill and include everything except email
db.users.find(
        {firstName: "Bill"},
        {
                "contact.email": 0
        }        
);

// Evaluation query operator

//$regex operator
db.users.find(
        {
                firstName: { $regex: "n", $options: "i"}
        }
);